# License: GPLv2 or GPLv3
# Copyright: Oleksandr Hnatiuk (aka Alex Const)

.PHONY: install enable reload

FILES = \
	usr.bin.skypeforlinux    \
	usr.bin.telegram-desktop

install:
	$(foreach profile,$(FILES),install -m 644 $(profile) $(DESTDIR)/etc/apparmor.d;)

enable: install
	$(foreach profile,$(FILES),apparmor_parser -a /etc/apparmor.d/$(profile);)

reload: install
	$(foreach profile,$(FILES),apparmor_parser -r /etc/apparmor.d/$(profile);)
