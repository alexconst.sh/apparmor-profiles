# apparmor-profiles
This is my personal collection of AppArmor profiles.

## Dependencies
Make sure AppArmor is installed and enabled on your system.
The following binary is required to activate the profiles: `apparmor_parser`.
Additionally, the following binaries are required to install via Makefile: `make` `install`.

## Installation
Install: `sudo make install`
Install and enable: `sudo make enable`

## Notes
These profiles are tested on Debian 11. If they don't work as expected on your system, please open an issue.
Please check out explanations / notes at the top of profiles before use.
By default, these profiles may be too strict for your use case.

## Additional security
Consider running untrusted programs such as Skype, Zoom, etc. inside an X11 sandbox such as Xephyr.
Otherwise any keypress may be logged and transmitted over the network.
Alternatively, use wayland. But note that all non-wayland programs utilizing Xwayland in a single wayland session will share access to the keyboard and clipboard.
For example, if you use urxvt and Skype, Skype will have access to urxvt window and vice versa.
